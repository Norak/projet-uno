package Default;

import javax.swing.*;

public class Carte {
    private String nomImage;
    private int typeCarte; // 0 = Chiffre et 1 = Spéciale
    private int valeurCarte; // Valeur de la carte chiffre ou 0 = Passer son Tour; 1 = Inversion; 2 = Prend 2; 3 = Joker; 4 = SuperJoker ou Prend4
    private String couleurCarte; // b, r, j, v ou n s'il s'agit d'une carte joker ou superjoker

    Carte(){

    }

    // ***** Getters et Setters ***** //

    public String getNomImage() {
        return nomImage;
    }

    public void setNomImage(String nomImage) {
        this.nomImage = nomImage;
    }

    public int getTypeCarte() {
        return typeCarte;
    }

    public void setTypeCarte(int typeCarte) {
        this.typeCarte = typeCarte;
    }

    public int getValeurCarte() {
        return valeurCarte;
    }

    public void setValeurCarte(int valeurCarte) {
        this.valeurCarte = valeurCarte;
    }

    public String getCouleurCarte() {
        return couleurCarte;
    }

    public void setCouleurCarte(String couleurCarte) {
        this.couleurCarte = couleurCarte;
    }
}
