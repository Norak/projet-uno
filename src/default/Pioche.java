package Default;

import javax.swing.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

/**
 * @author sokhnafatoudieng
 */

public class Pioche {


    private int nbTotalCartes = 108;
    private ArrayList <Carte> totalCartes ;




    public Pioche(){
        this.totalCartes = creationPioche(nbTotalCartes);
    }


    // ***** Méthode qui va initialiser la pioche ***** //
    private ArrayList <Carte> creationPioche(int nbTotalCartes) {

        totalCartes = new ArrayList<>(nbTotalCartes); // Création d'une liste de taille : nombre total de cartes (préalablement initialisée)
        File lot = new File("images/Cartes"); // Création d'un répertoire associé au répertoire contenant les cartes
        String lots[] = lot.list(); // Récupération de l'ensemble des fichiers du répertoire lot (Cartes) que l'on va stocker dans un tableau de string

        if(lots != null){ // Tant qu'on a pas parcouru tout les fichiers du répertoire :
            for (String file : lots){ //Aller au fichier suivant  et ...
                Carte element = new Carte();
                element.setNomImage(file);
                totalCartes.add(element);
                //... ajouter chaque fichier (image) à l'arraylist totalCartes
            }
        }
        return totalCartes; // Retourner la liste de cartes ainsi créée

    }



    // ***** Méthode qui va distibuer les cartes ***** //
    private  void distribuerCarte(Joueur joueur) {

        Carte carteADistribuer; // Initialiser la carte à distribuer
        Collections.shuffle(getTotalCartes()); // Mélanger la pioche
        carteADistribuer = getTotalCartes().get(0); //Prendre la première carte

        //Ajouter cette carte à distribuer au joueur correspondant

        joueur.getCartesJoueur().add(carteADistribuer);


        System.out.println("Nom du joueur \t" +joueur.getNomJoueur()+ "\t" + joueur.getCartesJoueur());

        getTotalCartes().remove(0); //L'effacer de la pioche
        setNbTotalCartes(getNbTotalCartes()-1); // Décrémenter le nombre total de cartes restantes.

    }







    public  void distribuerNCartes(int nombre, Joueur joueur) {
        if(nombre>0){
            distribuerCarte(joueur);
            nombre--;
            distribuerNCartes(nombre, joueur);
        }

    }

    public Carte initialiserTalon(){
        Carte carteADistribuer; // Initialiser la carte à distribuer
        Collections.shuffle(getTotalCartes()); // Mélanger la pioche
        carteADistribuer = getTotalCartes().get(0); //Prendre la première carte

        getTotalCartes().remove(0); //L'effacer de la pioche
        setNbTotalCartes(getNbTotalCartes()-1); // Décrémenter le nombre total de cartes restantes.

        return carteADistribuer;
    }

    public int getNbTotalCartes() {
        return nbTotalCartes;
    }

    public void setNbTotalCartes(int nbTotalCartes) {
        this.nbTotalCartes = nbTotalCartes;
    }

    public ArrayList<Carte> getTotalCartes() {
        return totalCartes;
    }

    public void setTotalCartes(ArrayList<Carte> totalCartes) {
        this.totalCartes = totalCartes;
    }
}
